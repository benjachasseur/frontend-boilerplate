var gulp        = require('gulp'),
    browserSync = require('browser-sync').create(),
    reload      = browserSync.reload,
    sass        = require('gulp-sass'),
    jshint      = require('gulp-jshint'),
    imagemin    = require('gulp-imagemin'),
    cache       = require('gulp-cache'),
    watch       = require('gulp-watch'),
    cleanCSS    = require('gulp-clean-css'),
    sourcemaps  = require('gulp-sourcemaps'),
    gutil       = require('gulp-util'),
    plumber     = require('gulp-plumber'),
    uglify      = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    include     = require("gulp-include");



// ---------------------------------------------------
// CSS
// ---------------------------------------------------
gulp.task('build-css', function() {
  return gulp.src('./assets/styles/**/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({debug: true}, function(details) {
        console.log(details.name + ': ' + details.stats.originalSize);
        console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('dist/styles/'))
    .pipe(reload({stream: true}));
});

// Same without sourcemaps.
gulp.task('build-css-prod', function() {
    return gulp.src('./assets/styles/**/*.scss')
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist/styles/'));
});



// ---------------------------------------------------
// JS
// ---------------------------------------------------
gulp.task('jshint', function() {
  return gulp.src('./assets/scripts/**/*.js')
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build-js', function() {
  return gulp.src('./assets/scripts/main.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(include().on('error', function(err){
        gutil.log(err);
        this.emit('end');
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/scripts/'))
    .pipe(reload({stream: true}));
});

// Same without sourcemaps.
gulp.task('build-js-prod', function() {
  return gulp.src('./assets/scripts/main.js')
    .pipe(plumber())
    .pipe(include().on('error', function(err){
        gutil.log(err);
        this.emit('end');
    }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts/'));
});



// ---------------------------------------------------
// IMAGES
// ---------------------------------------------------
gulp.task('images', function(){
  return gulp.src('assets/images/**/*.+(png|jpg|jpeg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist/images'))
});


// ---------------------------------------------------
// IMAGES
// ---------------------------------------------------
gulp.task('fonts', function() {
  return gulp.src('assets/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
});


// ---------------------------------------------------
// IMAGES
// ---------------------------------------------------
gulp.task('images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist/images'))
});



// ---------------------------------------------------
// WATCH
// ---------------------------------------------------
gulp.task('watch', function() {
  browserSync.init({
    server: {
        //"devUrl": "https://project-name.dev",
        baseDir: "./"
    }
  });
  gulp.watch('assets/styles/**/*.scss', ['build-css']);
  gulp.watch('assets/scripts/**/*.js', ['jshint', 'build-js']);
  gulp.watch('assets/fonts/**/*', ['fonts']);
  gulp.watch('assets/images/**/*', ['images']);
});



// ---------------------------------------------------
// DEFAULT
// ---------------------------------------------------
gulp.task('default', ['watch']);



// ---------------------------------------------------
// PROD
// ---------------------------------------------------
gulp.task('prod', [
    'jshint',
    'build-js-prod',
    'build-css-prod',
    'fonts',
    'images'
]);
