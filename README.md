# Frontend boilerplate using Gulp, SASS, Bourbon, Neat and BrowserSync
# 8757370eb965d3dd10046e76dc735550b8b5ccdc

## Included

1. [npm](https://www.npmjs.com/)
2. [Gulp](http://gulpjs.com/)
3. [Browsersync](https://www.browsersync.io/docs/gulp/)
4. [Bourbon](http://bourbon.io/)
5. [Neat](http://neat.bourbon.io/)

## Installation Instructions

1. ``git clone https://benjachasseur@bitbucket.org/benjachasseur/frontend-boilerplate.git``
2. ``cd frontend-boilerplate``
3. ``npm install`` (This assumes you have npm installed globally)

You now have all the necessary dependencies to run the build process.

## Build commands

* `gulp` — Compile assets when file changes are made, start Browsersync session
* `gulp prod` — Compile and optimize the files (styles, scripts, images, fonts) in your assets directory

## Using Browsersync

By default, Browsersync use current directory as Basedir. To use Browsersync with your local development hostname, you need to add `devUrl` at the line 141 of `gulpfile.js`.

If your local development URL is `https://project-name.dev`, update the file to read:
```js
...
  "devUrl": "https://project-name.dev",
...
```


## Contributong

Access needed... Ask me ¯\_(ツ)_/¯ !
